#include "../FinalVarsBoostedAlg.h"
#include "../FinalVarsResolvedAlg.h"
#include "../GhostAssocVRJetGetterAlg.h"
#include "../JetPairingAlg.h"
#include "../JetSelectorAlg.h"
#include "../JetTruthMatcherAlg.h"
#include "../LargeJetGhostVRJetAssociationAlg.h"
#include "../TruthParticleInformationAlg.h"
#include "../VariableDumperAlg.h"
#include "../tools/NTrkVertexCounter.h"

using namespace HH4B;

DECLARE_COMPONENT(FinalVarsBoostedAlg)
DECLARE_COMPONENT(FinalVarsResolvedAlg)
DECLARE_COMPONENT(GhostAssocVRJetGetterAlg)
DECLARE_COMPONENT(JetPairingAlg)
DECLARE_COMPONENT(JetSelectorAlg)
DECLARE_COMPONENT(JetTruthMatcherAlg)
DECLARE_COMPONENT(NTrkVertexCounter)
DECLARE_COMPONENT(VariableDumperAlg)
DECLARE_COMPONENT(TruthParticleInformationAlg)
DECLARE_COMPONENT(LargeJetGhostVRJetAssociationAlg)
