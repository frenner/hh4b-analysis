///////////////////////// -*- C++ -*- /////////////////////////////
// NTrkVertexCounter.h
// Header file for algorithm class NTrkVertexCounter
//
// This is an algorithm that will count the number of vertices given
// the number of NTrk, the minimum number of tracks.
//
// Author: T.J.Khoo<khoo@cern.ch>
///////////////////////////////////////////////////////////////////

// Always protect against multiple includes!
#ifndef HH4BANALYSIS_NTRKVERTEXCOUNTER_H
#define HH4BANALYSIS_NTRKVERTEXCOUNTER_H

// STL includes
#include <string>

// FrameWork includes
#include "AsgTools/AsgTool.h"

// Local includes
#include "HH4bAnalysis/IVertexCounter.h"

// EDM includes
#include "xAODTracking/VertexContainer.h"

// Forward declaration

namespace HH4B
{

  class NTrkVertexCounter final : virtual public IVertexCounter,
                                  public asg::AsgTool
  {
    ASG_TOOL_CLASS(NTrkVertexCounter, IVertexCounter)

    ///////////////////////////////////////////////////////////////////
    // Public methods:
    ///////////////////////////////////////////////////////////////////
public:
    /// Constructor with parameters:
    NTrkVertexCounter(const std::string &name);
    ~NTrkVertexCounter(){};

    // Athena algtool's Hooks
    StatusCode initialize();

    ///////////////////////////////////////////////////////////////////
    // Const methods:
    ///////////////////////////////////////////////////////////////////

    /// \brief Implements the interface specified in IVertexCounter
    size_t countVertices(const xAOD::VertexContainer &) const override;

    ///////////////////////////////////////////////////////////////////
    // Private methods:
    ///////////////////////////////////////////////////////////////////
private:
    bool accept(const xAOD::Vertex &) const;

    /// Default constructor:
    // NTrkVertexCounter();

    ///////////////////////////////////////////////////////////////////
    // Private data:
    ///////////////////////////////////////////////////////////////////

    // Member variables used for configuration of the tool
    // ~Always use size_t for counting -- it is unsigned and is large enough
    // to accommodate the size of any container
    size_t m_minNtrks;
  };

} //> end namespace HH4B
#endif //> !HH4BANALYSIS_NTRKVERTEXCOUNTER_H
